namespace Otus_Solid_HomeWork.Interfaces;

public interface IInputController
{
    Stack<int> LastInputs { get; }

    int? LastInput { get; }

    bool ProcessReadInputNumber();

    void ReadInputForExit();
}