using Otus_Solid_HomeWork.Core.Data;

namespace Otus_Solid_HomeWork.Interfaces;

public interface IGameStates
{
    event Action OnStateChanged;

    GameState CurrentState { get; }

    GameState PrevState { get; }

    void SetState(GameState state);

    void Initialize();
}