namespace Otus_Solid_HomeWork.Interfaces;

public interface IGameController
{
    void StartGame();
}