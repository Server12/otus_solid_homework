namespace Otus_Solid_HomeWork.Interfaces;

public interface IGameMessagesController
{
    void PrintMessage(string message);
}