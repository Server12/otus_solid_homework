using Otus_Solid_HomeWork.Core;
using Otus_Solid_HomeWork.Core.Data;

namespace Otus_Solid_HomeWork.Interfaces;

public interface IGameStatesFactory
{
    public BaseStateAction Create(GameState state,IGameStates states);
}