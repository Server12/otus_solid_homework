using Range = Otus_Solid_HomeWork.Core.Data.Range;

namespace Otus_Solid_HomeWork.Interfaces;

public interface INumberGenerator
{
    int? GeneratedNumber { get; }

    int GenerateNumber();
}