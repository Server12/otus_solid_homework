using Otus_Solid_HomeWork.Core.Data;
using Otus_Solid_HomeWork.Interfaces;

namespace Otus_Solid_HomeWork.Controllers;

public class GameController : IGameController
{
    private readonly IGameStates _states;

    public GameController(IGameStates states)
    {
        _states = states;
    }

    public void StartGame()
    {
        _states.Initialize();
        _states.SetState(GameState.GenerateNumber);
    }
}