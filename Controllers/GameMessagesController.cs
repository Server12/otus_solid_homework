using Otus_Solid_HomeWork.Interfaces;

namespace Otus_Solid_HomeWork.Controllers;

public class GameMessagesController : IGameMessagesController
{
    
    public void PrintMessage(string message)
    {
        Console.WriteLine(message);
    }
    
}