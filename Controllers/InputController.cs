using Otus_Solid_HomeWork.Interfaces;

namespace Otus_Solid_HomeWork.Controllers;

public class InputController : IInputController
{
    private readonly Stack<int> _lastInputs = new Stack<int>(10);

    public Stack<int> LastInputs => _lastInputs;

    public int? LastInput => _lastInputs.Count > 0 ? _lastInputs.Peek() : null;

    public void ReadInputForExit()
    {
        Console.ReadKey();
    }

    public bool ProcessReadInputNumber()
    {
      
        var input = Console.ReadLine();
        if (int.TryParse(input, out var lastInput))
        {
            _lastInputs.Push(lastInput);
            return true;
        }
        return false;
    }
}