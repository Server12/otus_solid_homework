﻿// See https://aka.ms/new-console-template for more information

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus_Solid_HomeWork.Controllers;
using Otus_Solid_HomeWork.Core;
using Otus_Solid_HomeWork.Core.Data;
using Otus_Solid_HomeWork.Core.Factory;
using Otus_Solid_HomeWork.Interfaces;

HostApplicationBuilder builder = Host.CreateApplicationBuilder(args);
builder.Services.AddScoped<IInputController, InputController>();
builder.Services.AddScoped<INumberGenerator, NumberGenerator>();
builder.Services.AddScoped<IGameMessagesController, GameMessagesController>();
builder.Services.AddScoped<IGameStates, GameStates>();
builder.Services.AddScoped<IGameStatesFactory, GameStatesFactory>();
builder.Services.AddScoped<IGameController, GameController>();


builder.Configuration.AddJsonFile("appsettings.json", true, true);

builder.Services.Configure<GameSettings>(builder.Configuration.GetSection("Settings"));

using IHost host = builder.Build();

using IServiceScope scope = host.Services.CreateScope();
var controller = scope.ServiceProvider.GetRequiredService<IGameController>();
controller.StartGame();

host.Run();