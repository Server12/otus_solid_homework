using Otus_Solid_HomeWork.Core.Data;
using Otus_Solid_HomeWork.Interfaces;

namespace Otus_Solid_HomeWork.Core;

public class GameOverState : BaseStateAction
{
    private readonly IInputController _inputController;
    private readonly IGameMessagesController _messagesController;
    private readonly INumberGenerator _numberGenerator;

    public GameOverState(IInputController inputController, IGameMessagesController messagesController,
        INumberGenerator numberGenerator,
        IGameStates states) : base(states)
    {
        _inputController = inputController;
        _messagesController = messagesController;
        _numberGenerator = numberGenerator;
    }

    public override void OnEnter()
    {
        if (_states.CurrentState == GameState.InputGuessed)
        {
            _messagesController.PrintMessage(
                $"{_inputController.LastInput} правильное число, вы угадали");
        }
        else if (_states.CurrentState == GameState.InputNotGuessed)
        {
            _messagesController.PrintMessage(
                $"вы не угадали число {_numberGenerator.GeneratedNumber}");
        }

        _messagesController.PrintMessage("Press any key to exit");
        _inputController.ReadInputForExit();
    }

    public override void OnExit()
    {
    }
}