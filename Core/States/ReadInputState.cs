using Otus_Solid_HomeWork.Core.Data;
using Otus_Solid_HomeWork.Interfaces;

namespace Otus_Solid_HomeWork.Core;

public class ReadInputState : BaseStateAction
{
    private readonly int _totalAttempts;
    private readonly IGameMessagesController _messagesController;
    private readonly IInputController _inputController;
    private readonly INumberGenerator _numberGenerator;

    private int _attempts = 0;

    public ReadInputState(int totalAttempts,
        IGameMessagesController messagesController,
        IInputController inputController,
        INumberGenerator numberGenerator,
        IGameStates states) : base(states)
    {
        _totalAttempts = totalAttempts;
        _messagesController = messagesController;
        _inputController = inputController;
        _numberGenerator = numberGenerator;
    }

    public override void OnEnter()
    {

        if (_attempts == _totalAttempts)
        {
            _states.SetState(GameState.InputNotGuessed);
            return;
        }
        
        _messagesController.PrintMessage("Введите число:");

        if (!_inputController.ProcessReadInputNumber())
        {
            _states.SetState(GameState.WrongInput);
            return;
        }

        var lastInput = _inputController.LastInput;
        var _targetNumber = _numberGenerator.GeneratedNumber;


        if (lastInput > _targetNumber)
        {
            _messagesController.PrintMessage($"{lastInput} больше загаданого числа");
            _states.SetState(GameState.ReadInput);
        }
        else if (lastInput < _targetNumber)
        {
            _messagesController.PrintMessage($"{lastInput} меньше загаданого числа");
            _states.SetState(GameState.ReadInput);
        }
        else
        {
            _states.SetState(GameState.InputGuessed);
        }
    }

    public override void OnExit()
    {
        if (_states.CurrentState != GameState.WrongInput)
        {
            _attempts++;
        }
    }
}