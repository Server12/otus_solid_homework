using Otus_Solid_HomeWork.Interfaces;

namespace Otus_Solid_HomeWork.Core;

public abstract class BaseStateAction:IDisposable
{
    protected readonly IGameStates _states;

    protected BaseStateAction(IGameStates states)
    {
        _states = states;
    }

    public abstract void OnEnter();

    public abstract void OnExit();

    public virtual void Dispose()
    {
        
    }
}