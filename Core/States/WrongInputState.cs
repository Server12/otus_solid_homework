using Otus_Solid_HomeWork.Core.Data;
using Otus_Solid_HomeWork.Interfaces;

namespace Otus_Solid_HomeWork.Core;

public class WrongInputState : BaseStateAction
{
    private readonly IGameMessagesController _messagesController;

    public WrongInputState(IGameMessagesController messagesController, IGameStates states) : base(states)
    {
        _messagesController = messagesController;
    }

    public override void OnEnter()
    {
        _messagesController.PrintMessage("Вы ввели не число");
        _states.SetState(GameState.ReadInput);
    }

    public override void OnExit()
    {
    }
}