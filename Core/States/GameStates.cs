using Otus_Solid_HomeWork.Core.Data;
using Otus_Solid_HomeWork.Interfaces;

namespace Otus_Solid_HomeWork.Core;

public sealed class GameStates : IGameStates, IDisposable
{
    private readonly IGameStatesFactory _factory;
    public event Action? OnStateChanged;

    public GameState PrevState { get; private set; } = GameState.None;

    public GameState CurrentState { get; private set; } = GameState.None;

    private bool _isInitialized;
    private GameState _state = GameState.None;

    private BaseStateAction _currentState;

    private readonly Dictionary<GameState, BaseStateAction> _states = new();

    public GameStates(IGameStatesFactory factory)
    {
        _factory = factory;
    }

    public void Initialize()
    {
        if (_isInitialized) return;
        _isInitialized = true;
        _states.Add(GameState.GenerateNumber, _factory.Create(GameState.GenerateNumber, this));
        _states.Add(GameState.ReadInput, _factory.Create(GameState.ReadInput, this));
        _states.Add(GameState.WrongInput, _factory.Create(GameState.WrongInput, this));
        _states.Add(GameState.InputGuessed, _factory.Create(GameState.InputGuessed, this));
        _states.Add(GameState.InputNotGuessed, _factory.Create(GameState.InputNotGuessed, this));
    }

    public void SetState(GameState gameState)
    {
        if (!_isInitialized) throw new Exception("GameStates Initialize first");

        PrevState = CurrentState;
        CurrentState = gameState;

        _currentState?.OnExit();

        if (_states.TryGetValue(gameState, out var state))
        {
            _currentState = state;
            _currentState.OnEnter();
        }

        OnStateChanged?.Invoke();
    }

    public void Dispose()
    {
        _states.Clear();
    }
}