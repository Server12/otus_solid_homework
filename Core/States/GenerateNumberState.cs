using Otus_Solid_HomeWork.Core.Data;
using Otus_Solid_HomeWork.Interfaces;

namespace Otus_Solid_HomeWork.Core;

public class GenerateNumberAction : BaseStateAction
{
    private readonly int _totalAttempts;
    private readonly IGameMessagesController _messagesController;
    private readonly INumberGenerator _numberGenerator;

    public GenerateNumberAction(int totalAttempts,
        IGameMessagesController messagesController,
        INumberGenerator numberGenerator,
        IGameStates states) : base(states)
    {
        _totalAttempts = totalAttempts;
        _messagesController = messagesController;
        _numberGenerator = numberGenerator;
    }

    public override void OnEnter()
    {
        _messagesController.PrintMessage($"Загадано число, дается:{_totalAttempts} попыток чтобы отгадать");
        _numberGenerator.GenerateNumber();
        _states.SetState(GameState.ReadInput);
    }

    public override void OnExit()
    {
    }
}