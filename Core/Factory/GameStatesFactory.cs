using Microsoft.Extensions.Options;
using Otus_Solid_HomeWork.Core.Data;
using Otus_Solid_HomeWork.Interfaces;

namespace Otus_Solid_HomeWork.Core.Factory;

public class GameStatesFactory : IGameStatesFactory
{
    private readonly IOptions<GameSettings> _options;
    private readonly IInputController _inputController;
    private readonly IGameMessagesController _messagesController;
    private readonly INumberGenerator _numberGenerator;

    public GameStatesFactory(IOptions<GameSettings> options,
        IInputController inputController,
        IGameMessagesController messagesController,
        INumberGenerator numberGenerator)
    {
        _options = options;
        _inputController = inputController;
        _messagesController = messagesController;
        _numberGenerator = numberGenerator;
    }


    public BaseStateAction Create(GameState state, IGameStates states)
    {
        switch (state)
        {
            case GameState.GenerateNumber:
                return new GenerateNumberAction(_options.Value.TotalAttempts, _messagesController, _numberGenerator,
                    states);
            case GameState.ReadInput:
                return new ReadInputState(_options.Value.TotalAttempts, _messagesController, _inputController,
                    _numberGenerator, states);
            case GameState.WrongInput:
                return new WrongInputState(_messagesController, states);
            case GameState.InputGuessed:
            case GameState.InputNotGuessed:
                return new GameOverState(_inputController, _messagesController,_numberGenerator, states);
            default:
                throw new ArgumentOutOfRangeException(nameof(state), state, null);
        }
    }
}