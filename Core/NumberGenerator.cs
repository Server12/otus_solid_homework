using Microsoft.Extensions.Options;
using Otus_Solid_HomeWork.Core.Data;
using Otus_Solid_HomeWork.Interfaces;
using Range = Otus_Solid_HomeWork.Core.Data.Range;

namespace Otus_Solid_HomeWork.Core;

public class NumberGenerator : INumberGenerator
{
    private readonly Random _random;
    public int? GeneratedNumber { get; private set; }

    private readonly int _min;
    private readonly int _max;

    public NumberGenerator(IOptions<GameSettings> settings)
    {
        _min = settings.Value.Min;
        _max = settings.Value.Max;
        if (settings.Value.Seed != null)
        {
            _random = new Random(settings.Value.Seed.Value);
        }
        else
        {
            _random = new Random();
        }
    }

    public int GenerateNumber()
    {
        if (GeneratedNumber != null) return GeneratedNumber.Value;
        GeneratedNumber = _random.Next(_min, _max);
        return GeneratedNumber.Value;
    }
}