namespace Otus_Solid_HomeWork.Core.Data;

public enum GameState
{
    None = 0,
    GenerateNumber,
    ReadInput,
    WrongInput,
    InputGuessed,
    InputNotGuessed,
}