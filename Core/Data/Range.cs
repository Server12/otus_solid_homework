namespace Otus_Solid_HomeWork.Core.Data;

[Serializable]
public struct Range
{
    public int min;
    public int max;
}