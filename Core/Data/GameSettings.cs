namespace Otus_Solid_HomeWork.Core.Data;

public sealed class GameSettings
{
    public int? Seed { get; set; }

    public int Min { get; set; }

    public int Max { get; set; }
    public int TotalAttempts { get; set; }
}